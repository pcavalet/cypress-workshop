const axios = require('axios')

module.exports = (on, config) => {
  on('task', {
    getCode() {
      return axios
        .get('https://us-central1-random-d85ea.cloudfunctions.net/codeWorkshop')
        .then((response) => response.data.code)
    },
  })
}
