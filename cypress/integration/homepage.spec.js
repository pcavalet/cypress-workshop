describe('Homepage', () => {
  it('Checks if everything is in the page', () => {
    cy.visit('/')
    cy.get('[data-cy="home-title"]').should('exist')
    cy.get('[data-cy="home-gif"]').should('exist')
  })
})
