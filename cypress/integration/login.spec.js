describe('Login', function() {
  it('Should log the user from home page', function() {
    cy.visit('/')
    cy.get('[data-cy="login-link"]').click()

    cy.get('[data-cy="login-gif"]').should('exist')
    cy.get('[data-cy="email-input"]').type('myemail@email.com')
    cy.get('[data-cy="password-input"]').type('*secretPassword*')
    cy.get('[data-cy="submit-button"]').click()
  })
})
