describe('Signup', () => {
  it('Should signup the user', () => {
    cy.visit(`/`)

    cy.get('[data-cy="signup1-link"]').click()
    cy.get(`[data-cy="signup-gif"]`).should('exist')
    cy.get('[data-cy="firstname-input"]').type('Jordan')
    cy.get('[data-cy="lastname-input"]').type('Jordan')
    cy.get('[data-cy="email-input"]').type('myemail@email.com')
    cy.get('[data-cy="password-input"]').type('*secretPassword*')
    cy.get('[data-cy="submit-button"]').click()

    cy.task('getCode').then((code) => {
      cy.get('[data-cy="code-input"]').type(code)
    })

    cy.get('[data-cy="submit-button"]').click()
  })
})
